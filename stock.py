from random import randint, uniform

from db import BaseDeDonnees

class Stock(BaseDeDonnees):
    """Gère une table "stock" pour une base de donnée donné."""
    def __init__(self):
        super().__init__(r"db.sqlite3") # connexion à la base de donnée

    def creationTable(self, presentation: bool = False) -> None:
        """Créer la table qui stocker les stocks."""
        requete = """
                  CREATE TABLE IF NOT EXISTS stocks (
                      id INTEGER PRIMARY KEY,
                      type TEXT,
                      nom TEXT,
                      quantite INTEGER,
                      prix REAL,
                      image_url TEXT
                  );
                  """
        self.requete(requete)
        # Ajout d'un stock par défaut si aucun stock n'existe dans la base de donnée
        if len(self.listeStocks()) == 0 and presentation:
            # Créer un dictionnaire d'éléments pour mieux voir ce que l'on ajoute à la base de donnée
            defaut = {
                "fruits legumes": [
                    ("banane", "img/banane.gif"),
                    ("orange", "img/orange.gif"),
                    ("betterave", "img/betterave.gif"),
                    ("carottes", "img/carottes.gif"),
                    ("tomates", "img/tomates.gif"),
                    ("citron", "img/citron.gif"),
                    ("kiwi", "img/kiwi.gif"),
                    ("clementine", "img/clementine.gif"),
                    ("pomme", "img/pomme.gif"),
                    ("avocat", "img/avocat.gif")
                ],
                "boulangerie": [
                    ("brownie", "img/brownie.gif"),
                    ("baguette", "img/baguette.gif"),
                    ("pain au chocolat", "img/pain_au_chocolat.gif"),
                    ("croissant", "img/croissant.gif"),
                    ("macaron", "img/macaron.gif"),
                    ("millefeuille", "img/millefeuille.gif"),
                    ("paris-brest", "img/paris-brest.gif"),
                    ("opera", "img/opera.gif"),
                    ("fraisier", "img/fraisier.gif"),
                    ("eclair", "img/eclair.gif")
                ],
                "boucherie poissonnerie": [
                    ("saucisson", "img/saucisson.gif"),
                    ("côte de boeuf", "img/cote_de_boeuf.gif"),
                    ("langue de boeuf", "img/langue_de_boeuf.gif"),
                    ("collier de boeuf", "img/collier_de_boeuf.gif"),
                    ("entrecote", "img/entrecote.gif"),
                    ("cabillaud", "img/cabillaud.gif"),
                    ("saumon", "img/saumon.gif"),
                    ("colin", "img/colin.gif"),
                    ("bar", "img/bar.gif"),
                    ("dorade", "img/dorade.gif")
                ],
                "entretien": [
                    ("nettoyant air comprimé", "img/nettoyant_air_comprime.gif"),
                    ("nettoyage anti-bactérien", "img/nettoyage_anti-bacterien.gif"),
                    ("nettoyant pour écran", "img/nettoyant_pour_ecran.gif"),
                    ("nettoyant pour lunettes", "img/nettoyant_pour_lunettes.gif"),
                    ("pioche", "img/pioche.gif"),
                    ("pelle", "img/pelle.gif"),
                    ("lampe torche", "img/lampe_torche.gif"),
                    ("gants", "img/gants.gif"),
                    ("éponge", "img/eponge.gif"),
                    ("essuie-tout", "img/essuie-tout.gif")
                ]
            }

            # Ajoute le dictionnaire précédemment créer à la base de donnée avec un prix et une quantité aléatoire
            for type in defaut:
                for element in defaut[type]:
                    self.ajoutStock(type, element[0], randint(0, 10), round(uniform(2., 30.), 2), element[1])

    def ajoutStock(self, typeElement: str, nom: str, quantite: int, prix: float, imageURL: str) -> list:
        """Ajoute un élément dans le stock et retourne l'ID de ce dernier."""
        requete = """
                  INSERT INTO stocks (
                      type, nom, quantite, prix, image_url
                  ) VALUES (
                      ?, ?, ?, ?, ?
                  );
                  """
        self.requete(requete, [typeElement.lower(), nom.lower(), quantite, prix, imageURL])
        return self.affichageResultat(self.requete("SELECT last_insert_rowid();"))

    def reduitQuantiteStock(self, id: int, quantiteARetirer: int) -> None:
        """Retire une quantité d'un élément du stock et met-à-jour la base de donnée."""
        requeteA = """
                  SELECT quantite FROM stocks
                  WHERE id = ?
                  """
        quantiteActuelle: int = self.affichageResultat(self.requete(requeteA, id))[0][0]
        if quantiteActuelle <= quantiteARetirer: # il ne reste plus rien
            quantiteFinale = 0
        else: # il reste quelque chose
            quantiteFinale = quantiteActuelle - quantiteARetirer
        # On met à jour la quantité de l'élément dans la base de donnée
        requeteB = """
                  UPDATE stocks
                  SET quantite = ?
                  WHERE id = ?
                  """
        self.requete(requeteB, [quantiteFinale, id])

    def listeStocks(self) -> list:
        """Retourne la liste des éléments en stock sous forme de dictionnaire."""
        recuperation = [
            "id",
            "type",
            "nom",
            "quantite",
            "prix",
            "image_url"
        ]
        requete = f"""
                   SELECT {", ".join(recuperation)} FROM stocks
                   """
        return [dict(zip(recuperation, element)) for element in self.affichageResultat(self.requete(requete))]

    def stockExistant(self, stock: str) -> bool:
        """Vérifie si le stock donnée existe déjà dans la base de donnée."""
        requete = """
                  SELECT EXISTS (
                      SELECT 1 FROM stocks
                      WHERE nom = ?
                  )
                  """
        return True if self.affichageResultat(self.requete(requete, stock.lower()))[0][0] == 1 else False

    def listeTypes(self) -> list:
        """Renvoie la liste des types disponibles dans la base de donnée."""
        requete = """
                  SELECT type FROM stocks
                  """
        res = []
        for i in self.affichageResultat(self.requete(requete)):
            if i[0] not in res:
                res.append(i[0])
        return res
