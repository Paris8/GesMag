import sqlite3

class BaseDeDonnees:
    """Gère la base de donnée."""
    def __init__(self, urlBaseDeDonnee: str):
        self.connexion = self.creerConnexion(urlBaseDeDonnee)

    def creerConnexion(self, path: str):
        """Connexion à une base de donnée SQLite."""
        if not self.fichierExiste(path): # si l base de donnée n'existe pas
            open(path, 'x') # on la créer
        try:
            connnexion = sqlite3.connect(path)
        except sqlite3.Error as e:
            print(e) # on affiche l'erreur
            connnexion = None # et renvoie None
        return connnexion

    def fichierExiste(self, path: str) -> bool:
        """Vérifie qu'un fichier existe."""
        try: # on essaie d'ouvrir le fichier
            open(path, 'r')
        except FileNotFoundError: # si le fichier n'existe pas
            return False
        else: # si le fichier existe
            return True

    def requete(self, requete: str, valeurs = None) -> tuple:
        """Envois une requête vers la base de données."""
        try:
            curseur = self.connexion.cursor()
            if valeurs: # s'il y a des valeurs alors on lance la commande `execute` avec ses dernières
                if type(valeurs) not in [list, tuple]: # si la valeur c'est juste une chaîne de charactère (par exemple), alors la converti en liste
                    valeurs = [valeurs]
                curseur.execute(requete, valeurs)
            else: # sinon on lance juste la requête
                curseur.execute(requete)
            self.connexion.commit() # applique les changements à la base de donnée
            return (curseur, curseur.lastrowid) # renvoie le curseur et l'ID de l'élément modifié
        except sqlite3.Error as e: # s'il y a eu une erreur SQLite
            print(e)

    def affichageResultat(self, curseur: tuple) -> list:
        """Affiche le résultat d'une requête."""
        tableau = []
        if curseur == None: # si le curseur est vide il n'y a rien a affiché (tableau vide)
            return tableau
        lignes = curseur[0].fetchall() # sinon on récupère les éléments
        for ligne in lignes:
            tableau.append(ligne) # on les ajoute au tableau
        return tableau # on le renvoie

    def affichageResultatDictionnaire(self, cles, curseur: sqlite3.Cursor) -> dict:
        """
        Même but que `affichageResultat()` mais avec
        les clés qui correspondent aux valeurs.
        """
        valeurs = self.affichageResultat(curseur)
        if len(valeurs) == 0:
            valeurs = []
        else:
            valeurs = valeurs[0]
        if type(cles) not in [list, tuple]:
            cles = [cles]
        if len(cles) != len(valeurs):
            raise IndexError # il y a pas autant de clés que de valeurs
        return dict(zip(cles, valeurs))
