# [Application "`GesMag`"](https://git.kennel.ml/Anri/GesMag)

Gestionnaire Manager/Caissier

Le compte par défaut créer lors de la création de la base de donnée est :

|                   |                                 |
|------------------:|---------------------------------|
| Nom d'utilisateur | admin                           |
|      Mot de passe | P@ssword                        |

Pour plus d'informations, [veuillez-vous référez à la documentation](https://git.kennel.ml/Anri/GesMag/media/branch/master/documentation/documentation.pdf)

Pour cloner le projet, vous devez avoir d'installer [Git LFS](https://git-lfs.github.com/) sur votre machine (-> pour les images).
