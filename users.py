from db import BaseDeDonnees

class Utilisateurs(BaseDeDonnees):
    """Gère une table "utilisateurs" pour une base de donnée donné."""
    def __init__(self):
        super().__init__(r"db.sqlite3") # connexion à la base de donnée

    def creationTable(self, presentation: bool = False) -> None:
        """Créer la table qui stocker les utilisateurs."""
        requete = """
                  CREATE TABLE IF NOT EXISTS utilisateurs (
                      id INTEGER PRIMARY KEY,
                      pseudo TEXT,
                      passe TEXT,
                      metier INTEGER,
                      nom TEXT,
                      prenom TEXT,
                      naissance TEXT,
                      adresse TEXT,
                      postal INTEGER
                  );
                  """
        self.requete(requete)
        # Ajout d'un utilisateur par défaut si aucun utilisateur n'existe dans la base de donnée
        if len(self.listUtilisateurs()) == 0 and presentation:
            self.ajoutUtilisateur(
                pseudo="admin",
                passe="P@ssword",
                metier=0,
                nom="Admin",
                prenom="Système",
                naissance="2000/10/09",
                adresse="12 Rue de Montmartre",
                postal=46800
            )
            print("--  Compte par défaut --\nNom d'utilisateur: admin\nMot de passe: P@ssword")

    def ajoutUtilisateur(self, pseudo: str, passe: str, metier: int, nom: str, prenom: str, naissance: str, adresse: str, postal: int) -> list:
        """Ajoute un utilisateur et retourne l'ID de ce dernier."""
        requete = """
                  INSERT INTO utilisateurs (
                      pseudo, passe, metier, nom, prenom, naissance, adresse, postal
                  ) VALUES (
                      ?, ?, ?, ?, ?, ?, ?, ?
                  );
                  """
        self.requete(requete, [pseudo.lower(), passe, metier, nom.upper(), prenom.capitalize(), naissance, adresse, postal])
        return self.affichageResultat(self.requete("SELECT last_insert_rowid();"))

    def suppressionUtilisateurs(self, pseudo: str) -> None:
        """Supprime un utilisateur."""
        requete = """
                  DELETE FROM utilisateurs
                  WHERE pseudo = ?
                  """
        self.requete(requete, pseudo)

    def verificationIdentifiants(self, pseudo: str, motDePasse: str) -> tuple:
        """
        Retourne l'ID de l'utilisateur si trouvé dans la base de donnée ainsi
        que son métier (`tuple`), sinon renvoie `(0,)`.
        """
        requete = """
                  SELECT id, metier FROM utilisateurs
                  WHERE pseudo = ? AND passe = ?
                  """
        reponseBaseDeDonnee = self.affichageResultat(self.requete(requete, [pseudo.lower(), motDePasse]))
        if len(reponseBaseDeDonnee) == 0: # si les identifiants renseignés sont mauvais
            return (0,)
        return reponseBaseDeDonnee[0]

    def listUtilisateurs(self) -> list:
        """Retourne la liste des nom d'utilisateurs (avec leur métier)."""
        requete = """
                  SELECT pseudo, metier FROM utilisateurs
                  """
        return self.affichageResultat(self.requete(requete))

    def recuperationUtilisateur(self, id: int = None, pseudo: str = None) -> dict:
        """Retourne les informations d'un utilisateur grâce à son ID ou son pseudo (ID en priorité)."""
        recuperation = [
            "id",
            "pseudo",
            "passe",
            "metier",
            "nom",
            "prenom",
            "naissance",
            "adresse",
            "postal",
        ]
        if not id: # si la variable `id` n'est pas définie
            if not pseudo: # si seul la variable `pseudo` n'est pas définie
                raise ValueError # Aucun utilisateur renseigné
            else: # si un pseudo est renseigné, c'est ce qu'on va utilisé
                requete = f"""
                           SELECT {", ".join(recuperation)} FROM utilisateurs
                           WHERE pseudo = ?
                           """
            utilisateur = pseudo
        else: # si un id est renseigné, c'est ce qu'on va utilisé
            requete = f"""
                       SELECT {", ".join(recuperation)} FROM utilisateurs
                       WHERE id = ?
                       """
            utilisateur = id
        return self.affichageResultatDictionnaire(recuperation, self.requete(requete, utilisateur))

    def utilisateurExistant(self, utilisateur: str) -> bool:
        """Vérifie si l'utilisateur donnée existe déjà dans la base de donnée."""
        requete = """
                  SELECT EXISTS (
                      SELECT 1 FROM utilisateurs
                      WHERE pseudo = ?
                  )
                  """
        return True if self.affichageResultat(self.requete(requete, utilisateur.lower()))[0][0] == 1 else False
